# WEIGHTS = {
#     'Consumo_agua_No_registrada_Benimamet': {
#         'KNN': 0.18455888528197192,
#         'Zscore': 0.24929122452568514,
#         'DBSCAN': 0.24929122452568514,
#         'IsolationForest': 0.14860425792893642,
#         'LOF': 0.16825440773772135
#     },
#     'Consumo_agua_No_registrada_Exposicio': {
#         'KNN': 0.10690811223541588,
#         'Zscore': 0.2318170999666505,
#         'DBSCAN': 0.2357131856803758,
#         'IsolationForest': 0.23277899664701007,
#         'LOF': 0.19278260547054787
#     },
#     'Consumo_agua_No_registrada_Malilla': {
#         'KNN': 0.09720232082922366,
#         'Zscore': 0.24193044096359156,
#         'DBSCAN': 0.2502585112205138,
#         'IsolationForest': 0.23264031203058963,
#         'LOF': 0.1779684149560814
#     },
#     'Consumo_agua_No_registrada_Mestalla': {
#         'KNN': 0.11648848130119234,
#         'Zscore': 0.22547780864695363,
#         'DBSCAN': 0.2367261347019037,
#         'IsolationForest': 0.23045524371642284,
#         'LOF': 0.1908523316335275
#     },
#     'Consumo_agua_No_registrada_Perellonet': {
#         'Zscore': 0.06371399058203896,
#         'DBSCAN': 0.8034178812418084,
#         'IsolationForest': 0.03738948141987984,
#         'LOF': 0.09547864675627286
#     },
#     'Consumo_agua_No_registrada_Soternes': {
#         'Zscore': 0.027219855337306822,
#         'DBSCAN': 0.7931302675870441,
#         'IsolationForest': 0.11219891590255744,
#         'LOF': 0.06745096117309168
#     },
#     'Consumo_agua_ServMunicipal_benimamet': {
#         'IsolationForest': 0.46358543417366943,
#         'LOF': 0.5364145658263305
#     },
#     'Consumo_agua_ServMunicipal_exposicio': {
#         'IsolationForest': 0.6290322580645162,
#         'LOF': 0.3709677419354838
#     },
#     'Consumo_agua_ServMunicipal_malilla': {
#         'Zscore': 0.5615542054116777,
#         'IsolationForest': 0.3391565002981423,
#         'LOF': 0.09928929429018003
#     },
#     'Consumo_agua_ServMunicipal_mestalla': {
#         'IsolationForest': 0.6268115942028993,
#         'LOF': 0.3731884057971007
#     },
#     'Consumo_agua_ServMunicipal_perellonet': {
#         'IsolationForest': 0.6291512915129159,
#         'LOF': 0.37084870848708396
#     },
#     'Consumo_agua_ServMunicipal_soternes': {
#         'IsolationForest': 0.7654867256637172,
#         'LOF': 0.23451327433628266
#     },
#     'Consumo_agua_domestico_benimamet': {},
#     'Consumo_agua_domestico_exposicio': {},
#     'Consumo_agua_domestico_malilla': {
#         'Zscore': 0.01831033929574066,
#         'DBSCAN': 0.9557997112376677,
#         'IsolationForest': 0.012037779738509665,
#         'LOF': 0.013852169728082037
#     },
#     'Consumo_agua_domestico_mestalla': {},
#     'Consumo_agua_domestico_perellonet': {
#         'DBSCAN': 0.9619654460990021,
#         'IsolationForest': 0.023929488708930406,
#         'LOF': 0.014105065192067431
#     },
#     'Consumo_agua_domestico_soternes': {
#         'Zscore': 0.06893946778438446,
#         'DBSCAN': 0.8962130811969979,
#         'IsolationForest': 0.02185885563895117,
#         'LOF': 0.012988595379666538
#     },
#     'Consumo_agua_industrial_benimamet': {},
#     'Consumo_agua_industrial_exposicio': {},
#     'Consumo_agua_industrial_malilla': {
#         'IsolationForest': 0.7666666666666677,
#         'LOF': 0.23333333333333225
#     },
#     'Consumo_agua_industrial_mestalla': {},
#     'Consumo_agua_industrial_perellonet': {
#         'IsolationForest': 0.7645739910313906,
#         'LOF': 0.23542600896860952
#     },
#     'Consumo_agua_industrial_soternes': {
#         'KNN': 0.021363261613904106,
#         'Zscore': 0.48280971247423304,
#         'DBSCAN': 0.48280971247423304,
#         'IsolationForest': 0.006020071227858267,
#         'LOF': 0.0069972422097714405
#     }
# }

WEIGHTS = {
    'Consumo_agua_No_registrada_Benimamet': {
        'KNN': 0.15446878616899695,
        'Zscore': 0.21667378384245792,
        'DBSCAN': 0.20198403578534208,
        'IsolationForest': 0.21532798394281527,
        'LOF': 0.21154541026038787
    },
    'Consumo_agua_No_registrada_Exposicio': {
        'KNN': 0.10147865943611986,
        'Zscore': 0.22352270806569158,
        'DBSCAN': 0.22352270806569158,
        'IsolationForest': 0.2270286136447137,
        'LOF': 0.22444731078778335
    },
    'Consumo_agua_No_registrada_Malilla': {
        'KNN': 0.09456267398749686,
        'Zscore': 0.22194526312755597,
        'DBSCAN': 0.22341509930720868,
        'IsolationForest': 0.23058254184040178,
        'LOF': 0.22949442173733678
    },
    'Consumo_agua_No_registrada_Mestalla': {
        'KNN': 0.11183550655994992,
        'Zscore': 0.21783757597196488,
        'DBSCAN': 0.21783757597196488,
        'IsolationForest': 0.2278821183241387,
        'LOF': 0.22460722317198156
    },
    'Consumo_agua_No_registrada_Perellonet': {
        'KNN': 0.4075207197916237,
        'Zscore': 0.019706030937699404,
        'DBSCAN': 0.09702874280752945,
        'IsolationForest': 0.3087278180239573,
        'LOF': 0.16701668843919004
    },
    'Consumo_agua_No_registrada_Soternes': {
        'KNN': 0.3250017134824559,
        'Zscore': 0.01342982287117585,
        'DBSCAN': 0.16977701450576055,
        'IsolationForest': 0.3145177872410863,
        'LOF': 0.17727366189952137
    },
    'Consumo_agua_ServMunicipal_benimamet': {
        'KNN': 0.029402131840647876,
        'Zscore': 0.2490360566902876,
        'DBSCAN': 0.23642663609837433,
        'IsolationForest': 0.2964714960598663,
        'LOF': 0.18866367931082398
    },
    'Consumo_agua_ServMunicipal_exposicio': {
        'KNN': 0.04627774867227546,
        'Zscore': 0.2822942669008804,
        'DBSCAN': 0.2396838115196154,
        'IsolationForest': 0.2822942669008804,
        'LOF': 0.14944990600634842
    },
    'Consumo_agua_ServMunicipal_malilla': {
        'KNN': 0.49171914665760613,
        'Zscore': 0.04030484808668899,
        'DBSCAN': 0.1446232784287077,
        'IsolationForest': 0.2731773036986701,
        'LOF': 0.050175423128327144
    },
    'Consumo_agua_ServMunicipal_mestalla': {
        'KNN': 0.46975131997904157,
        'Zscore': 0.11184555237596228,
        'DBSCAN': 0.06347990810527589,
        'IsolationForest': 0.260972955543912,
        'LOF': 0.09395026399580828
    },
    'Consumo_agua_ServMunicipal_perellonet': {
        'KNN': 0.5520462205103516,
        'Zscore': 0.042465093885411656,
        'DBSCAN': 0.042465093885411656,
        'IsolationForest': 0.3066923447279731,
        'LOF': 0.05633124699085218
    },
    'Consumo_agua_ServMunicipal_soternes': {
        'KNN': 0.4661220940164304,
        'Zscore': 0.09322441880328605,
        'DBSCAN': 0.10133089000357182,
        'IsolationForest': 0.2589567188980169,
        'LOF': 0.08036587827869487
    },
    'Consumo_agua_domestico_benimamet': {
        'KNN': 0.2,
        'Zscore': 0.2,
        'DBSCAN': 0.2,
        'IsolationForest': 0.2,
        'LOF': 0.2
    },
    'Consumo_agua_domestico_exposicio': {
        'KNN': 0.030982668650585722,
        'Zscore': 0.4160529790221514,
        'DBSCAN': 0.26476098665046,
        'IsolationForest': 0.24269757109625503,
        'LOF': 0.0455057945805478
    },
    'Consumo_agua_domestico_malilla': {
        'KNN': 0.5559556337333762,
        'Zscore': 0.01065049106768914,
        'DBSCAN': 0.06779946752846051,
        'IsolationForest': 0.30886424096298676,
        'LOF': 0.05673016670748735
    },
    'Consumo_agua_domestico_mestalla': {
        'KNN': 0.2,
        'Zscore': 0.2,
        'DBSCAN': 0.2,
        'IsolationForest': 0.2,
        'LOF': 0.2
    },
    'Consumo_agua_domestico_perellonet': {
        'KNN': 0.5907455624472032,
        'Zscore': 0.003615333919505509,
        'DBSCAN': 0.1181491124894406,
        'IsolationForest': 0.22720983171046275,
        'LOF': 0.06028015943338807
    },
    'Consumo_agua_domestico_soternes': {
        'KNN': 0.5244022066546599,
        'Zscore': 0.04033863128112768,
        'DBSCAN': 0.09041417356114823,
        'IsolationForest': 0.29133455925258883,
        'LOF': 0.053510429250475486
    },
    'Consumo_agua_industrial_benimamet': {
        'KNN': 0.2,
        'Zscore': 0.2,
        'DBSCAN': 0.2,
        'IsolationForest': 0.2,
        'LOF': 0.2
    },
    'Consumo_agua_industrial_exposicio': {
        'KNN': 0.3453406421299922,
        'Zscore': 0.3453406421299922,
        'DBSCAN': 0.0822239624119029,
        'IsolationForest': 0.19185591229444013,
        'LOF': 0.03523884103367266
    },
    'Consumo_agua_industrial_malilla': {
        'KNN': 0.3499444532613871,
        'Zscore': 0.3499444532613871,
        'DBSCAN': 0.0699888906522774,
        'IsolationForest': 0.19441358514521506,
        'LOF': 0.03570861767973337
    },
    'Consumo_agua_industrial_mestalla': {
        'KNN': 0.2,
        'Zscore': 0.2,
        'DBSCAN': 0.2,
        'IsolationForest': 0.2,
        'LOF': 0.2
    },
    'Consumo_agua_industrial_perellonet': {
        'KNN': 0.3986285647017276,
        'Zscore': 0.015450719562082464,
        'DBSCAN': 0.221460313723182,
        'IsolationForest': 0.28473468907266253,
        'LOF': 0.07972571294034549
    },
    'Consumo_agua_industrial_soternes': {
        'KNN': 0.021207115055535037,
        'Zscore': 0.47928080025509207,
        'DBSCAN': 0.18433876932888155,
        'IsolationForest': 0.2662671112528289,
        'LOF': 0.04890620410766244
    }
}


PARAMS_DICT = {
    'Consumo_agua_No_registrada_Benimamet': {
        'KNN': {'n_neighbors': 3},
        'IsolationForest': {'n_estimators': 300, 'max_samples': 0.75,
                            'contamination': 0.17185714285714285},
        'DBSCAN': {'min_samples': 14, 'eps': 0.7000000000000001},
        'LOF': {'n_neighbors': 14, 'contamination': 0.07444444444444444}
    },
    'Consumo_agua_No_registrada_Exposicio': {
        'KNN': {'n_neighbors': 3},
        'IsolationForest': {'n_estimators': 500, 'max_samples': 0.5,
                            'contamination': 0.022357142857142857},
        'DBSCAN': {'min_samples': 14, 'eps': 0.7000000000000001},
        'LOF': {'n_neighbors': 14, 'contamination': 0.07444444444444444}
    },
    'Consumo_agua_No_registrada_Malilla': {
        'KNN': {'n_neighbors': 3},
        'IsolationForest': {'n_estimators': 400, 'max_samples': 'auto',
                            'contamination': 0.04371428571428571},
        'DBSCAN': {'min_samples': 14, 'eps': 0.7000000000000001},
        'LOF': {'n_neighbors': 14, 'contamination': 0.07444444444444444}
    },
    'Consumo_agua_No_registrada_Mestalla': {
        'KNN': {'n_neighbors': 3},
        'IsolationForest': {'n_estimators': 500, 'max_samples': 0.25,
                            'contamination': 0.022357142857142857},
        'DBSCAN': {'min_samples': 14, 'eps': 0.7000000000000001},
        'LOF': {'n_neighbors': 14, 'contamination': 0.07444444444444444}
    },
    'Consumo_agua_No_registrada_Perellonet': {
        'KNN': {'n_neighbors': 3},
        'IsolationForest': {'n_estimators': 250, 'max_samples': 'auto',
                            'contamination': 0.1932142857142857},
        'DBSCAN': {'min_samples': 14, 'eps': 0.7000000000000001},
        'LOF': {'n_neighbors': 14, 'contamination': 0.07444444444444444}
    },
    'Consumo_agua_No_registrada_Soternes': {
        'KNN': {'n_neighbors': 3},
        'IsolationForest': {'n_estimators': 400, 'max_samples': 'auto',
                            'contamination': 0.04371428571428571},
        'DBSCAN': {'min_samples': 14, 'eps': 0.7000000000000001},
        'LOF': {'n_neighbors': 14, 'contamination': 0.07444444444444444}
    },
    'Consumo_agua_ServMunicipal_benimamet': {
        'KNN': {'n_neighbors': 3},
        'IsolationForest': {'n_estimators': 300, 'max_samples': 0.5,
                            'contamination': 0.08642857142857142},
        'DBSCAN': {'min_samples': 14, 'eps': 0.7000000000000001},
        'LOF': {'n_neighbors': 14, 'contamination': 0.07444444444444444}
    },
    'Consumo_agua_ServMunicipal_exposicio': {
        'KNN': {'n_neighbors': 3},
        'IsolationForest': {'n_estimators': 400, 'max_samples': 'auto',
                            'contamination': 0.04371428571428571},
        'DBSCAN': {'min_samples': 14, 'eps': 0.7000000000000001},
        'LOF': {'n_neighbors': 14, 'contamination': 0.07444444444444444}
    },
    'Consumo_agua_ServMunicipal_malilla': {
        'KNN': {'n_neighbors': 3},
        'IsolationForest': {'n_estimators': 50, 'max_samples': 1.0,
                            'contamination': 0.022357142857142857},
        'DBSCAN': {'min_samples': 14, 'eps': 0.7000000000000001},
        'LOF': {'n_neighbors': 14, 'contamination': 0.07444444444444444}
    },
    'Consumo_agua_ServMunicipal_mestalla': {
        'KNN': {'n_neighbors': 3},
        'IsolationForest': {'n_estimators': 400, 'max_samples': 1.0,
                            'contamination': 0.04371428571428571},
        'DBSCAN': {'min_samples': 14, 'eps': 0.7000000000000001},
        'LOF': {'n_neighbors': 14, 'contamination': 0.07444444444444444}
    },
    'Consumo_agua_ServMunicipal_perellonet': {
        'KNN': {'n_neighbors': 3},
        'IsolationForest': {'n_estimators': 300, 'max_samples': 0.75,
                            'contamination': 0.04371428571428571},
        'DBSCAN': {'min_samples': 14, 'eps': 0.7000000000000001},
        'LOF': {'n_neighbors': 14, 'contamination': 0.07444444444444444}
    },
    'Consumo_agua_ServMunicipal_soternes': {
        'KNN': {'n_neighbors': 3},
        'IsolationForest': {'n_estimators': 500, 'max_samples': 0.25,
                            'contamination': 0.022357142857142857},
        'DBSCAN': {'min_samples': 14, 'eps': 0.7000000000000001},
        'LOF': {'n_neighbors': 14, 'contamination': 0.07444444444444444}
    },
    'Consumo_agua_domestico_benimamet': {
        'KNN': {'n_neighbors': 3},
        'IsolationForest': {'n_estimators': 500, 'max_samples': 0.25,
                            'contamination': 0.022357142857142857},
        'DBSCAN': {'min_samples': 14, 'eps': 0.7000000000000001},
        'LOF': {'n_neighbors': 14, 'contamination': 0.07444444444444444}
    },
    'Consumo_agua_domestico_exposicio': {
        'KNN': {'n_neighbors': 3},
        'IsolationForest': {'n_estimators': 300, 'max_samples': 0.75,
                            'contamination': 0.04371428571428571},
        'DBSCAN': {'min_samples': 14, 'eps': 0.7000000000000001},
        'LOF': {'n_neighbors': 14, 'contamination': 0.07444444444444444}
    },
    'Consumo_agua_domestico_malilla': {
        'KNN': {'n_neighbors': 3},
        'IsolationForest': {'n_estimators': 50, 'max_samples': 1.0,
                            'contamination': 0.08642857142857142},
        'DBSCAN': {'min_samples': 14, 'eps': 0.7000000000000001},
        'LOF': {'n_neighbors': 14, 'contamination': 0.07444444444444444}
    },
    'Consumo_agua_domestico_mestalla': {
        'KNN': {'n_neighbors': 3},
        'IsolationForest': {'n_estimators': 300, 'max_samples': 1.0,
                            'contamination': 0.022357142857142857},
        'DBSCAN': {'min_samples': 14, 'eps': 0.7000000000000001},
        'LOF': {'n_neighbors': 14, 'contamination': 0.07444444444444444}
    },
    'Consumo_agua_domestico_perellonet': {
        'KNN': {'n_neighbors': 3},
        'IsolationForest': {'n_estimators': 50, 'max_samples': 0.75,
                            'contamination': 0.04371428571428571},
        'DBSCAN': {'min_samples': 14, 'eps': 0.7000000000000001},
        'LOF': {'n_neighbors': 14, 'contamination': 0.07444444444444444}
    },
    'Consumo_agua_domestico_soternes': {
        'KNN': {'n_neighbors': 3},
        'IsolationForest': {'n_estimators': 50, 'max_samples': 0.75,
                            'contamination': 0.04371428571428571},
        'DBSCAN': {'min_samples': 14, 'eps': 0.7000000000000001},
        'LOF': {'n_neighbors': 14, 'contamination': 0.07444444444444444}
    },
    'Consumo_agua_industrial_benimamet': {
        'KNN': {'n_neighbors': 3},
        'IsolationForest': {'n_estimators': 200, 'max_samples': 1.0,
                            'contamination': 0.12914285714285714},
        'DBSCAN': {'min_samples': 14, 'eps': 0.7000000000000001},
        'LOF': {'n_neighbors': 14, 'contamination': 0.07444444444444444}
    },
    'Consumo_agua_industrial_exposicio': {
        'KNN': {'n_neighbors': 3},
        'IsolationForest': {'n_estimators': 50, 'max_samples': 1.0,
                            'contamination': 0.08642857142857142},
        'DBSCAN': {'min_samples': 14, 'eps': 0.7000000000000001},
        'LOF': {'n_neighbors': 14, 'contamination': 0.07444444444444444}
    },
    'Consumo_agua_industrial_malilla': {
        'KNN': {'n_neighbors': 3},
        'IsolationForest': {'n_estimators': 300, 'max_samples': 1.0,
                            'contamination': 0.022357142857142857},
        'DBSCAN': {'min_samples': 14, 'eps': 0.7000000000000001},
        'LOF': {'n_neighbors': 14, 'contamination': 0.07444444444444444}
    },
    'Consumo_agua_industrial_mestalla': {
        'KNN': {'n_neighbors': 3},
        'IsolationForest': {'n_estimators': 50, 'max_samples': 0.75,
                            'contamination': 0.04371428571428571},
        'DBSCAN': {'min_samples': 14, 'eps': 0.7000000000000001},
        'LOF': {'n_neighbors': 14, 'contamination': 0.07444444444444444}
    },
    'Consumo_agua_industrial_perellonet': {
        'KNN': {'n_neighbors': 3},
        'IsolationForest': {'n_estimators': 500, 'max_samples': 0.25,
                            'contamination': 0.022357142857142857},
        'DBSCAN': {'min_samples': 14, 'eps': 0.7000000000000001},
        'LOF': {'n_neighbors': 14, 'contamination': 0.07444444444444444}
    },
    'Consumo_agua_industrial_soternes': {
        'KNN': {'n_neighbors': 3},
        'IsolationForest': {'n_estimators': 400, 'max_samples': 0.5,
                            'contamination': 0.08642857142857142},
        'DBSCAN': {'min_samples': 14, 'eps': 0.7000000000000001},
        'LOF': {'n_neighbors': 14, 'contamination': 0.07444444444444444}
    }
}
