sensor_id_mapping = {
    "Consumo_agua_ServMunicipal_exposicio": "kpidesagcorregido-desag-exposicio-servMunicipal",
    "Consumo_agua_ServMunicipal_malilla": "kpidesagcorregido-malilla-servMunicipal",
    "Consumo_agua_ServMunicipal_perellonet": "kpidesagcorregido-perellonet-servMunicipal",
    "Consumo_agua_ServMunicipal_soternes": "kpidesagcorregido-soternes-servMunicipal",
    "Consumo_agua_ServMunicipal_mestalla": "kpidesagcorregido-mestalla-servMunicipal",
    "Consumo_agua_ServMunicipal_benimamet": "kpidesagcorregido-benimamet-servMunicipal",

    "Consumo_agua_industrial_exposicio": "kpidesagcorregido-exposicio-industrial",
    "Consumo_agua_industrial_malilla": "kpidesagcorregido-malilla-industrial",
    "Consumo_agua_industrial_perellonet": "kpidesagcorregido-perellonet-industrial",
    "Consumo_agua_industrial_soternes": "kpidesagcorregido-soternes-industrial",
    "Consumo_agua_industrial_mestalla": "kpidesagcorregido-mestalla-industrial",
    "Consumo_agua_industrial_benimamet": "kpidesagcorregido-benimamet-industrial",

    "Consumo_agua_domestico_exposicio": "kpidesagcorregido-exposicio-domestico",
    "Consumo_agua_domestico_malilla": "kpidesagcorregido-malilla-domestico",
    "Consumo_agua_domestico_perellonet": "kpidesagcorregido-perellonet-domestico",
    "Consumo_agua_domestico_soternes": "kpidesagcorregido-soternes-domestico",
    "Consumo_agua_domestico_mestalla": "kpidesagcorregido-mestalla-domestico",
    "Consumo_agua_domestico_benimamet": "kpidesagcorregido-benimamet-domestico",

    "Consumo_agua_No_registrada_Perellonet": "kpidesagcorregido-perellonet-no-registrada",
    "Consumo_agua_No_registrada_Mestalla": "kpidesagcorregido-mestalla-no-registrada",
    "Consumo_agua_No_registrada_Malilla": "kpidesagcorregido-malilla-no-registrada",
    "Consumo_agua_No_registrada_Soternes": "kpidesagcorregido-soternes-no-registrada",
    "Consumo_agua_No_registrada_Exposicio": "kpidesagcorregido-exposicio-no-registrada",
    "Consumo_agua_No_registrada_Benimamet": "kpidesagcorregido-benimamet-no-registrada",

}
description_mapping = {

    "Consumo_agua_ServMunicipal_exposicio": "Caudal de agua consumido en el barrio de exposicio servMunicipal",
    "Consumo_agua_ServMunicipal_malilla": "Caudal de agua consumido en el barrio de malilla servMunicipal",
    "Consumo_agua_ServMunicipal_perellonet": "Caudal de agua consumido en el barrio de perellonet servMunicipal",
    "Consumo_agua_ServMunicipal_soternes": "Caudal de agua consumido en el barrio de soternes servMunicipal",
    "Consumo_agua_ServMunicipal_mestalla": "Caudal de agua consumido en el barrio de mestalla servMunicipal",
    "Consumo_agua_ServMunicipal_benimamet": "Caudal de agua consumido en el barrio de benimamet servMunicipal",

    "Consumo_agua_industrial_exposicio": "Caudal de agua consumido en el barrio de exposicio industrial",
    "Consumo_agua_industrial_malilla": "Caudal de agua consumido en el barrio de malilla industrial",
    "Consumo_agua_industrial_perellonet": "Caudal de agua consumido en el barrio de perellonet industrial",
    "Consumo_agua_industrial_soternes": "Caudal de agua consumido en el barrio de soternes industrial",
    "Consumo_agua_industrial_mestalla": "Caudal de agua consumido en el barrio de mestalla industrial",
    "Consumo_agua_industrial_benimamet": "Caudal de agua consumido en el barrio de benimamet industrial",

    "Consumo_agua_domestico_exposicio": "Caudal de agua consumido en el barrio de exposicio domestico",
    "Consumo_agua_domestico_malilla": "Caudal de agua consumido en el barrio de malilla domestico",
    "Consumo_agua_domestico_perellonet": "Caudal de agua consumido en el barrio de perellonet domestico",
    "Consumo_agua_domestico_soternes": "Caudal de agua consumido en el barrio de soternes domestico",
    "Consumo_agua_domestico_mestalla": "Caudal de agua consumido en el barrio de mestalla domestico",
    "Consumo_agua_domestico_benimamet": "Caudal de agua consumido en el barrio de benimamet domestico",

    "Consumo_agua_No_registrada_Perellonet": "Caudal de agua consumido en el barrio de perellonet no-registrada",
    "Consumo_agua_No_registrada_Mestalla": "Caudal de agua consumido en el barrio de mestalla no-registrada",
    "Consumo_agua_No_registrada_Malilla": "Caudal de agua consumido en el barrio de malilla no-registrada",
    "Consumo_agua_No_registrada_Soternes": "Caudal de agua consumido en el barrio de soternes no-registrada",
    "Consumo_agua_No_registrada_Exposicio": "Caudal de agua consumido en el barrio de exposicio no-registrada",
    "Consumo_agua_No_registrada_Benimamet": "Caudal de agua consumido en el barrio de benimamet no-registrada"
}
name_mapping = {
    'valencia': 'Valencia ciudad'
}
