from math import e

import pandas as pd


def convert_dates(raw_data_df, log):
    """
    Converts the 'calculationperiod' column of the raw
    DataFrame to datetime format and sets it as the index.

    Parameters:
    - raw_data_df (pd.DataFrame): The raw data DataFrame.
    - log (Logger): Logger instance for logging messages.

    Returns:
    - pd.DataFrame or None: The DataFrame with the converted
    'calculationperiod' column or None if an error occurs.
    """
    try:
        raw_data_df['calculationperiod'] = pd.to_datetime(
            raw_data_df['calculationperiod'], format='%d/%m/%Y')
        raw_data_df.set_index('calculationperiod', inplace=True)
        raw_data_df.sort_index(inplace=True)
        return raw_data_df
    except Exception as e:
        log.logger.error(
            f"Error converting calculationperiod to datetime: {e}")
        return None


def prepare_name_data(group, log):
    """
    Prepares the time series data for a single graph by removing
    duplicates and handling missing values.

    Parameters:
    - group (pd.DataFrame): DataFrame grouped by name.
    - log (Logger): Logger instance for logging messages.

    Returns:
    - pd.DataFrame: The processed DataFrame for the graph.
    """
    if group.empty:
        log.logger.error(f"No data available for name. {e}")
        return group
    time_series_data = group[['original_kpivalue']]
    time_series_data = time_series_data[~time_series_data.index.duplicated(
        keep='first')]
    time_series_data['filled'] = time_series_data['original_kpivalue'].isna()
    time_series_data['original_kpivalue'] = time_series_data[
        'original_kpivalue'].asfreq(
        'D').bfill()
    return time_series_data


def preprocess_time_series(raw_data_df, log):
    """
    Preprocesses raw data to prepare a time series
    DataFrame for each graph.

    Parameters:
    - raw_data_df (pd.DataFrame): The raw data DataFrame.
    - log (Logger): Logger instance for logging messages.

    Returns:
    - dict: A dictionary where keys are neighborhoods
    and each value is a DataFrame.
    """
    if raw_data_df.empty:
        log.logger.error("Input data frame is empty.")
        return {}

    processed_df = convert_dates(raw_data_df, log)
    if processed_df is None:
        return {}

    name_dataframes = {}
    for name, group in processed_df.groupby('name'):
        prepared_data = prepare_name_data(group, log)
        if not prepared_data.empty:
            name_dataframes[name] = prepared_data

    return name_dataframes


def main_preprocessor(raw_data_df, log):
    """
    Main function to run preprocessing on specified raw data.

    Returns:
    - dict: A dictionary of preprocessed data frames by neighborhood.
    """
    log.logger.info("Data preprocessing started...")
    neighborhood_dataframes = preprocess_time_series(raw_data_df, log)
    log.logger.info("Data preprocessing completed.")
    return neighborhood_dataframes
