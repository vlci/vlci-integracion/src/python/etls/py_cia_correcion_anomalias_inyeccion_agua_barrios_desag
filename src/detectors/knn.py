import numpy as np
from robustbase.stats import Sn
from sklearn.neighbors import NearestNeighbors


def detect_anomalies_with_knn(time_series_data, best_params):
    data = time_series_data.copy()
    data = data.asfreq('D', method='pad')

    median = data['original_kpivalue'].median()
    sn_scale = Sn(data['original_kpivalue'].values)
    kpivalue_scaled = (data['original_kpivalue'].values - median) / sn_scale
    kpivalue_scaled = kpivalue_scaled.reshape(-1, 1)

    knn = NearestNeighbors(**best_params['KNN'])
    knn.fit(kpivalue_scaled)
    distances, indices = knn.kneighbors(kpivalue_scaled)
    kth_distances = distances[:, knn.n_neighbors - 1]

    threshold = 3 * np.mean(kth_distances)
    data['Anomaly_KNN'] = kth_distances > threshold

    # Mark all negative kpivalue values as anomalies
    data.loc[data['original_kpivalue'] <= 0, 'Anomaly_KNN'] = True

    return data
