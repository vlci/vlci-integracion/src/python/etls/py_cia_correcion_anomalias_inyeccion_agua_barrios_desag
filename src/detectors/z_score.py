from robustbase.stats import Sn


def detect_anomalies_with_zscore(time_series_data):
    data = time_series_data.copy()

    # Calculate the median of the data
    median = data['original_kpivalue'].median()

    # Calculate the Sn scale estimator
    sn_scale = Sn(data['original_kpivalue'].values)

    # Calculate the z-scores using the median and Sn scale estimator
    data['z_score'] = (data['original_kpivalue'] - median) / sn_scale

    # Define the threshold for anomaly detection
    threshold = 3.5

    # Identify anomalies based on the z-score with Sn scale estimator
    data['Anomaly_Zscore'] = abs(data['z_score']) > threshold

    # Mark all negative kpivalue values as anomalies
    data.loc[data['original_kpivalue'] <= 0, 'Anomaly_Zscore'] = True

    return data
