import unittest
from unittest.mock import MagicMock, patch

import pandas as pd

from detectors.detector import (calculate_anomaly_score, combine_anomalies,
                                confirm_anomalies, ensure_numeric,
                                main_detector)


class TestAnomalyDetection(unittest.TestCase):

    def test_ensure_numeric(self):
        df = pd.DataFrame({'original_kpivalue': ['1', '2', 'a', '4']})
        result = ensure_numeric(df)
        expected = pd.DataFrame({'original_kpivalue': [1.0, 2.0, 4.0]})
        pd.testing.assert_frame_equal(result.reset_index(drop=True), expected)

    def test_combine_anomalies(self):
        time_series_data = pd.DataFrame({'original_kpivalue': [1, 2, 3, 4]})
        anomalies_results = {
            'KNN': pd.DataFrame({'Anomaly_KNN': [0, 1, 0, 1]}),
            'Zscore': pd.DataFrame({'Anomaly_Zscore': [1, 0, 1, 0]})
        }
        result = combine_anomalies(time_series_data, anomalies_results)
        expected = pd.DataFrame({
            'Original_Value': [1, 2, 3, 4],
            'Anomaly_KNN': [0, 1, 0, 1],
            'Anomaly_Zscore': [1, 0, 1, 0]
        })
        pd.testing.assert_frame_equal(result, expected)

    def test_calculate_anomaly_score(self):
        anomalies_df_combined = pd.DataFrame({
            'Original_Value': [1, 2, 3, 4],
            'Anomaly_KNN': [0, 1, 0, 1],
            'Anomaly_Zscore': [1, 0, 1, 0]
        })

        weights = {'KNN': 0.6, 'Zscore': 0.4}
        result = calculate_anomaly_score(anomalies_df_combined, weights)

        expected = pd.DataFrame({
            'Original_Value': [1, 2, 3, 4],
            'Anomaly_KNN': [0, 0, 0, 0],  # Converted back to int
            'Anomaly_Zscore': [0, 0, 0, 0],  # Converted back to int
            # The anomaly score will be float64
            'Anomaly_Score': [0.4, 0.6, 0.4, 0.6]
        })

        # Update the expected DataFrame's data types to match the actual output
        expected['Anomaly_KNN'] = expected['Anomaly_KNN'].astype(int)
        expected['Anomaly_Zscore'] = expected['Anomaly_Zscore'].astype(int)

        pd.testing.assert_frame_equal(result, expected)

    def test_confirm_anomalies(self):
        anomalies_df_combined = pd.DataFrame({
            'Original_Value': [1, 2, 3, 4],
            'Anomaly_KNN': [0, 0, 0, 0],
            'Anomaly_Zscore': [0, 0, 0, 0],
            'Anomaly_Score': [0.4, 0.6, 0.4, 0.6]
        })
        result = confirm_anomalies(anomalies_df_combined)

        expected = pd.DataFrame({
            'Original_Value': [1, 2, 3, 4],
            'Anomaly_KNN': [0, 0, 0, 0],
            'Anomaly_Zscore': [0, 0, 0, 0],
            'Anomaly_Score': [0.4, 0.6, 0.4, 0.6],
            'Confirmed_Anomaly': [False, True, False, True]
        })

        pd.testing.assert_frame_equal(result, expected)

    @patch('detectors.detector.detect_anomalies_with_knn')
    @patch('detectors.detector.detect_anomalies_with_zscore')
    @patch('detectors.detector.detect_anomalies_with_dbscan')
    @patch('detectors.detector.detect_with_isolation_forest')
    @patch('detectors.detector.detect_anomalies_with_lof')
    def test_main_detector(
        self,
        mock_lof,
        mock_iforest,
        mock_dbscan,
        mock_zscore,
        mock_knn
    ):
        mock_logger = MagicMock()
        mock_logger.logger = MagicMock()

        # Mock the return values for anomaly detection methods
        mock_knn.return_value = pd.DataFrame({'Anomaly_KNN': [0, 1, 0, 1]})
        mock_zscore.return_value = pd.DataFrame(
            {'Anomaly_Zscore': [1, 0, 1, 0]})
        mock_dbscan.return_value = pd.DataFrame(
            {'Anomaly_DBSCAN': [0, 0, 1, 1]})
        mock_iforest.return_value = pd.DataFrame(
            {'Anomaly_IsolationForest': [1, 0, 0, 1]})
        mock_lof.return_value = pd.DataFrame({'Anomaly_LOF': [0, 1, 1, 0]})

        neighborhood_desag_dataframes = {
            'neighborhood1': pd.DataFrame({'original_kpivalue': [1, 2, 3, 4]})
        }

        with patch('config.models_params.PARAMS_DICT', {'neighborhood1': {}}):
            with patch('config.models_params.WEIGHTS', {'neighborhood1': {
                'KNN': 0.6,
                'Zscore': 0.4,
                'DBSCAN': 0.3,
                'IsolationForest': 0.5,
                'LOF': 0.2
            }}):
                result = main_detector(
                    neighborhood_desag_dataframes, mock_logger)

        expected_combined_df = pd.DataFrame({
            'Original_Value': [1, 2, 3, 4],
            'Anomaly_KNN': [0, 1, 0, 1],
            'Anomaly_Zscore': [1, 0, 1, 0],
            'Anomaly_DBSCAN': [0, 0, 1, 1],
            'Anomaly_IsolationForest': [1, 0, 0, 1],
            'Anomaly_LOF': [0, 1, 1, 0],
            'Anomaly_Score': [2.0, 2.0, 3.0, 3.0],
            'Confirmed_Anomaly': [True, True, True, True]
        })

        pd.testing.assert_frame_equal(
            result['neighborhood1'], expected_combined_df)


if __name__ == '__main__':
    unittest.main()
