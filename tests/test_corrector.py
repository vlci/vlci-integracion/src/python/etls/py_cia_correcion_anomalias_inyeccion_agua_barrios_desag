import unittest
from unittest.mock import MagicMock

import pandas as pd
from pandas.testing import assert_series_equal

# Import the module to test
from corrector import (assign_season, calculate_weekday_medians,
                       correct_all_anomalies, correct_single_anomaly,
                       initialize_columns, main_corrector)


class TestAnomalyCorrection(unittest.TestCase):

    def setUp(self):
        # Create a sample DataFrame with a datetime index for testing
        date_range = pd.date_range(start="2023-01-01", periods=10, freq='D')
        data = {
            'Original_Value': [100, 105, 98, 102, 110, 103, 99, 101, 107, 104],
            'Confirmed_Anomaly': [False, True, False,
                                  True, False, False,
                                  True, False, False,
                                  True]
        }
        self.df = pd.DataFrame(data, index=date_range)
        self.df.index.name = 'Date'

    def test_initialize_columns(self):
        initialize_columns(self.df)

        # Test if 'dayofweek', 'season', and 'Corrected_Data_Conditional'
        # columns are added
        self.assertIn('dayofweek', self.df.columns)
        self.assertIn('season', self.df.columns)
        self.assertIn('Corrected_Data_Conditional', self.df.columns)

        # Test if 'dayofweek' is correctly computed
        # Sunday is 7, Monday is 1, etc.
        expected_days = [7, 1, 2, 3, 4, 5, 6, 7, 1, 2]
        self.assertListEqual(self.df['dayofweek'].tolist(), expected_days)

        # Test if 'season' is correctly assigned
        self.assertTrue(all(self.df['season'] == 'winter'))

        # Test if 'Corrected_Data_Conditional' matches 'Original_Value'
        self.assertTrue(
            self.df['Corrected_Data_Conditional'].equals(
                self.df['Original_Value'])
        )

    def test_assign_season(self):
        winter_date = pd.Timestamp("2023-01-01")
        spring_date = pd.Timestamp("2023-04-01")
        summer_date = pd.Timestamp("2023-07-01")
        fall_date = pd.Timestamp("2023-10-01")

        self.assertEqual(assign_season(winter_date), 'winter')
        self.assertEqual(assign_season(spring_date), 'spring')
        self.assertEqual(assign_season(summer_date), 'summer')
        self.assertEqual(assign_season(fall_date), 'fall')

    def test_calculate_weekday_medians(self):
        initialize_columns(self.df)
        medians = calculate_weekday_medians(self.df)

        # Ensure dayofweek is int64 to match expected data type
        medians.index = medians.index.set_levels(
            [medians.index.levels[0], medians.index.levels[1].astype('int64')])

        # We know the non-anomalous values are [100, 98, 110, 103, 101, 107]
        expected_medians = pd.Series(
            [107, 98, 110, 103, 100.5],
            index=pd.MultiIndex.from_tuples(
                [('winter', 1), ('winter', 2), ('winter', 4), ('winter', 5),
                 ('winter', 7)],
                names=['season', 'dayofweek']
            ),
            name='Original_Value'
        )
        assert_series_equal(medians, expected_medians)

    def test_correct_single_anomaly(self):
        initialize_columns(self.df)
        weekday_medians = calculate_weekday_medians(self.df)

        row = self.df.loc[self.df.index[1]]  # An anomalous value
        correct_single_anomaly(self.df, self.df.index[1], row, weekday_medians)

        # Test if the anomaly is corrected
        self.assertEqual(
            self.df.at[self.df.index[1], 'Corrected_Data_Conditional'], 107.0)
        self.assertEqual(
            self.df.at[self.df.index[1], 'Corrected_Data_Final'], 107.0)

    def test_correct_all_anomalies(self):
        corrected_df = correct_all_anomalies(self.df.copy())

        # Check if all anomalies are corrected correctly
        expected_corrected_values = [
            # Updated to match output
            100.0, 107.0, 98.0, 102.0, 110.0, 103.0, 99.0, 101.0, 107.0, 98.0]
        self.assertListEqual(
            corrected_df['Corrected_Data_Final'].tolist(),
            expected_corrected_values)

    def test_main_corrector(self):
        log_mock = MagicMock()
        neighborhood_data = {'neighborhood1': self.df.copy()}

        corrected_dataframes = main_corrector(neighborhood_data, log_mock)

        # Ensure that the log messages were called
        log_mock.logger.info.assert_called()

        # Test if the returned dictionary has the corrected DataFrame
        corrected_df = corrected_dataframes['neighborhood1']
        expected_corrected_values = [
            # Updated to match output
            100.0, 107.0, 98.0, 102.0, 110.0, 103.0, 99.0, 101.0, 107.0, 98.0]
        self.assertListEqual(
            corrected_df['Corrected_Data_Final'].tolist(),
            expected_corrected_values)


if __name__ == '__main__':
    unittest.main()
