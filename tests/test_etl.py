import unittest
from unittest.mock import MagicMock, patch

from etl import main


class TestETLProcess(unittest.TestCase):

    @patch('etl.main_load_local')
    @patch('etl.main_postprocess')
    @patch('etl.main_corrector')
    @patch('etl.main_detector')
    @patch('etl.main_preprocessor')
    @patch('etl.main_extractor')
    @patch('etl.FlowControl')
    @patch('etl.Logger')
    def test_main_success(
            self,
            mock_logger_cls,
            mock_flow_control_cls,
            mock_main_extractor,
            mock_main_preprocessor,
            mock_main_detector,
            mock_main_corrector,
            mock_main_postprocess,
            mock_main_load_local):
        # Setup mocks
        mock_logger = MagicMock()
        mock_logger_cls.return_value = mock_logger

        mock_flow_control = MagicMock()
        mock_flow_control_cls.return_value = mock_flow_control

        mock_main_extractor.return_value = 'raw_data_df'
        mock_main_preprocessor.return_value = 'neighborhood_desag_dataframes'
        mock_main_detector.return_value = 'dataframes_with_flagged_anomalies'
        mock_main_corrector.return_value = 'corrected_dataframes'
        mock_main_postprocess.return_value = 'ready_to_load_data'

        # Call the main function
        main()

        # Asserts
        mock_logger.logger.info.assert_called_with("Starting ETL process...")
        mock_main_extractor.assert_called_once_with(mock_logger)
        mock_main_preprocessor.assert_called_once_with(
            'raw_data_df', mock_logger)
        mock_main_detector.assert_called_once_with(
            'neighborhood_desag_dataframes', mock_logger)
        mock_main_corrector.assert_called_once_with(
            'dataframes_with_flagged_anomalies', mock_logger)
        mock_main_postprocess.assert_called_once_with(
            'corrected_dataframes', 'raw_data_df', mock_logger)
        mock_main_load_local.assert_called_once_with(
            'ready_to_load_data', mock_logger)
        mock_flow_control.end_exec.assert_called_once()


if __name__ == "__main__":
    unittest.main()
