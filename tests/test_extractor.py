import unittest
from unittest.mock import MagicMock, patch

import pandas as pd

from config.config import (DB_NAME, DB_PASSWORD, DB_USERNAME, HOST, PORT,
                           QUERY_EXTRACT_DATA, SCHEMA)
from extractor import main_extractor, read_from_db


class TestExtractor(unittest.TestCase):

    @patch('extractor.create_engine')
    @patch('extractor.pd.read_sql_query')
    def test_read_from_db(self, mock_read_sql_query, mock_create_engine):
        # Setup the mock engine and query results
        mock_engine = MagicMock()
        mock_create_engine.return_value = mock_engine

        mock_chunk1 = pd.DataFrame({'col1': [1, 2], 'col2': ['A', 'B']})
        mock_chunk2 = pd.DataFrame({'col1': [3, 4], 'col2': ['C', 'D']})
        mock_read_sql_query.return_value = [mock_chunk1, mock_chunk2]

        query = "SELECT * FROM some_table"

        # Expected values from config
        connection_string = (
            f'postgresql://{DB_USERNAME}:{DB_PASSWORD}'
            f'@{HOST}:{PORT}/{DB_NAME}'
        )
        connect_args = {'options': f'-csearch_path={SCHEMA}'}

        # Call the function
        result = read_from_db(query)

        # Assertions
        mock_create_engine.assert_called_once_with(
            connection_string, connect_args=connect_args)
        mock_read_sql_query.assert_called_once_with(
            query, mock_engine, chunksize=2000)
        pd.testing.assert_frame_equal(
            result, pd.concat([mock_chunk1, mock_chunk2]))

    @patch('extractor.read_from_db')
    def test_main_extractor(self, mock_read_from_db):
        # Setup the mock logger and query results
        mock_logger = MagicMock()
        mock_logger.logger = MagicMock()
        mock_df = pd.DataFrame({'col1': [1, 2, 3], 'col2': ['A', 'B', 'C']})
        mock_read_from_db.return_value = mock_df

        # Expected SQL query from config
        expected_query = QUERY_EXTRACT_DATA.strip()

        # Call the function
        result = main_extractor(mock_logger)

        # Assertions
        mock_logger.logger.info.assert_any_call("Data extraction started...")
        actual_query = mock_read_from_db.call_args[0][0].strip()
        self.assertEqual(actual_query, expected_query)
        mock_logger.logger.info.assert_any_call("Data extraction completed.")
        pd.testing.assert_frame_equal(result, mock_df)


if __name__ == "__main__":
    unittest.main()
